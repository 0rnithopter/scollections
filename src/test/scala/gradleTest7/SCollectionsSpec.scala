package gradleTest7

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.must
import SCollections.Functions
import SCollections.Decode

class SCollectionsSpec extends AnyFunSpec with must.Matchers {
  /*
  def checkCollection[T](original: Seq[T], newSeq: Seq[T]): Boolean = {
    newSeq.intersect(original).length == newSeq.length
  }
   */

  describe("isPalindrome") {
    describe("When it gets an empty collection") {
      it("returns true") {
        val input = Nil
        val expected = true
        val actual = input.isPalindrome

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one item") {
      it("returns true") {
        val input = Seq(1)
        val expected = true
        val actual = input.isPalindrome

        actual mustBe expected
      }
    }

    describe("When it gets a collection that contains a repetition of a single item") {
      describe("and that collection's size is even") {
        it("returns true") {
          val input = Seq(1, 1, 1, 1, 1, 1)
          val expected = true
          val actual = input.isPalindrome

          actual mustBe expected
        }
      }
      describe("and that collection's size is odd") {
        it("returns true") {
          val input = Seq(1, 1, 1, 1, 1)
          val expected = true
          val actual = input.isPalindrome

          actual mustBe expected
        }
      }
    }

    describe("When it gets a collection with a palindrome") {
      it("returns true") {
        val input = Seq(1, 2, 2, 1)
        val expected = true
        val actual = input.isPalindrome

        actual mustBe expected
      }
    }

    describe("When it get a collection without a palindrome") {
      it("returns false") {
        val input = Seq(1, 2)
        val expected = false
        val actual = input.isPalindrome

        actual mustBe expected
      }
    }
  }

  describe("compress") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val input = Nil
        val expected = Nil
        val actual = input.compress

        actual mustBe expected
      }
    }

    describe("When it gets a collection with a single item") {
      it("returns a collection with that item") {
        val input = Seq("a")
        val expected = Seq("a")
        val actual = input.compress

        actual mustBe expected
      }
    }

    describe("When it gets a collection with a repetition of a single item") {
      it("returns a collection with one instance of the item") {
        val input = Seq("a", "a", "a", "a")
        val expected = Seq("a")
        val actual = input.compress

        actual mustBe expected
      }
    }

    describe("When it gets a collection without sequences") {
      it("returns the input collection") {
        val input = Seq('a', 'b', 'c', 'a', 'b', 'c', 'a', 'b')
        val expected = Seq('a', 'b', 'c', 'a', 'b', 'c', 'a', 'b')
        val actual = input.compress

        actual mustBe expected
      }
    }

    describe("When it gets a collection with several sequences") {
      it("returns a collection with the input collection but without sequences") {
        val input = Seq('a', 'b', 'c', 'c', 'c', 'd', 'd', 'a')
        val expected = Seq('a', 'b', 'c', 'd', 'a')
        val actual = input.compress

        actual mustBe expected
      }
    }
  }

  describe("pack") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val input = Nil
        val expected = Nil
        val actual = input.pack

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one item") {
      it("returns collection(collection(item))") {
        val input = Seq('1')
        val expected = Seq(Seq('1'))
        val actual = input.pack

        actual mustBe expected
      }
    }

    describe("When it gets a collection with a repetition of a single item") {
      it("returns a collection with the input collection") {
        val input = Seq("a", "a", "a", "a")
        val expected = Seq(Seq("a", "a", "a", "a"))
        val actual = input.pack

        actual mustBe expected
      }
    }

    describe("When it gets a collection without sequences") {
      it("returns a collection with (a collection with the item) for each item in the original input") {
        val input = Seq("a", "b", "c", "d")
        val expected = Seq(Seq("a"), Seq("b"), Seq("c"), Seq("d"))
        val actual = input.pack

        actual mustBe expected
      }
    }

    describe("When it gets a collection with several sequences ('a', 'a', 'b', 'b', 'b', 'b', 'c', 'c', 'a', 'a', 'a', 'a')") {
      it("returns a collection with collections of all the sequences in the input (('a', 'a'), ('b', 'b', 'b', 'b'), ('c', 'c'), ('a', 'a', 'a', 'a'))") {
        val input = Seq('a', 'a', 'b', 'b', 'b', 'b', 'c', 'c', 'a', 'a', 'a', 'a', 'd', 'd', 'e')
        val expected = Seq(Seq('a', 'a'), Seq('b', 'b', 'b', 'b'), Seq('c', 'c'), Seq('a', 'a', 'a', 'a'), Seq('d', 'd'), Seq('e'))
        val actual = input.pack

        actual mustBe expected
      }
    }
  }

  describe("encode") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val input = Nil
        val expected = Nil
        val actual = input.encode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one item") {
      it("returns a collection with (1,item)") {
        val input = Seq('a')
        val expected = Seq((1, 'a'))
        val actual = input.encode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with n repetitions of a single item") {
      it("returns a collection with one instance of (n,item)") {
        val input = Seq("a", "a", "a", "a")
        val expected = Seq((4, "a"))
        val actual = input.encode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with unique items") {
      it("returns a collection with (1, item) for every item in the input") {
        val input = Seq("a", "b", "c", "d")
        val expected = Seq((1, "a"), (1, "b"), (1, "c"), (1, "d"))
        val actual = input.encode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with several sequences ('a', 'a', 'b', 'b', 'b', 'b', 'c', 'c', 'a', 'a', 'a', 'a', 'd', 'd', 'e')") {
      it("returns a collection with a tuple of (sequence.length,item) for each sequence in the input ((2, 'a'), (4, 'b'), (2, 'c'), (4, 'a'), (2, 'd'), (1, 'e')) ") {
        val input = Seq('a', 'a', 'b', 'b', 'b', 'b', 'c', 'c', 'a', 'a', 'a', 'a', 'd', 'd', 'e')
        val expected = Seq((2, 'a'), (4, 'b'), (2, 'c'), (4, 'a'), (2, 'd'), (1, 'e'))
        val actual = input.encode

        actual mustBe expected
      }
    }
  }

  describe("decode") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val input = Nil
        val expected = Nil
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets 0 as the number of repetitions - (0,item)") {
      it("returns an empty collection") {
        val input = Seq((0, 'a'))
        val expected = Nil
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets a negative number of repetitions - (-1,item)") {
      it("returns an empty collection") {
        val input = Seq((-1, 'a'))
        val expected = Nil
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one tuple of (1, item)") {
      it("returns a collection with the item") {
        val input = Seq((1, "a"))
        val expected = Seq("a")
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with many tuples of (1, item)") {
      it("returns a collection with the items") {
        val input = Seq((1, "a"), (1, "b"), (1, "c"), (1, "d"))
        val expected = Seq("a", "b", "c", "d")
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets a collection with several tuples of (repetitions,item) ((1, 'a'), (2, 'b'), (2, 'c'))") {
      it("returns a collection where each tuple (n,item) in the inputs turns into n instances of item ('a','b','b','c','c')") {
        val input = Seq((1, 'a'), (2, 'b'), (2, 'c'))
        val expected = Seq('a', 'b', 'b', 'c', 'c')
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets a combination of (0,item) and other tuples - ((1, 'a'), (0, 'b'), (1, 'c'))") {
      it("returns a collection where each tuple (n,item) in the inputs turns into n instances of item - ('a','c')") {
        val input = Seq((1, 'a'), (0, 'b'), (1, 'c'))
        val expected = Seq('a', 'c')
        val actual = input.decode

        actual mustBe expected
      }
    }

    describe("When it gets a combination of (-1,item) and other tuples - ((1, 'a'), (-1, 'a'), (1, 'c'))") {
      it("returns a collection where each tuple (n,item) in the inputs turns into max(0,n) instances of item - ('a','c')") {
        val input = Seq((1, 'a'), (-1, 'b'), (1, 'c'))
        val expected = Seq('a', 'c')
        val actual = input.decode

        actual mustBe expected
      }
    }
  }

  describe("duplicate") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val input = Nil
        val expected = Nil
        val actual = input.duplicate

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one item") {
      it("returns a collection with 2 instances of that item") {
        val input = Seq(1)
        val expected = Seq(1, 1)
        val actual = input.duplicate

        actual mustBe expected
      }
    }

    describe("When it gets a collection with a repetition of a single item") {
      it("returns a collection twice the size of the input collection") {
        val input = Seq(1, 1, 1, 1)
        val expected = Seq(1, 1, 1, 1, 1, 1, 1, 1)
        val actual = input.duplicate

        actual mustBe expected
      }
    }

    describe("When it gets a collection with several items (1, 2, 3)") {
      it("returns a collection with duplicated items (1, 1, 2, 2, 3, 3) ") {
        val input = Seq(1, 2, 3)
        val expected = Seq(1, 1, 2, 2, 3, 3)
        val actual = input.duplicate

        actual mustBe expected
      }
    }
  }

  describe("dropS") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val inputSeq = Nil
        val inputVal = 4
        val expected = Nil
        val actual = inputSeq.dropS(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets n bigger than the original collection size (n=5, size=3)") {
      it("returns the input collection") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 5
        val expected = Seq(1, 2, 3)
        val actual = inputSeq.dropS(inputVal)

        actual mustBe expected
      }
    }

    describe("When (n=1)") {
      describe("and the collection size is 1") {
        it("returns an empty collection") {
          val inputSeq = Seq(1)
          val inputVal = 1
          val expected = Nil
          val actual = inputSeq.dropS(inputVal)

          actual mustBe expected
        }
      }
      describe("and the collection size is larger than 1") {
        it("returns an empty collection") {
          val inputSeq = Seq(1, 2, 3, 4, 5, 6)
          val inputVal = 1
          val expected = Nil
          val actual = inputSeq.dropS(inputVal)

          actual mustBe expected
        }
      }
    }

    describe("When it gets a collection and a number smaller than the collection's size (size=8,n=2) (1, 2, 3, 4, 5, 6, 7, 8)") {
      it("returns the input collection without every n-th item (1, 3, 5, 7)") {
        val inputSeq = Seq(1, 2, 3, 4, 5, 6, 7, 8)
        val inputVal = 2
        val expected = Seq(1, 3, 5, 7)
        val actual = inputSeq.dropS(inputVal)

        actual mustBe expected
      }
    }
  }

  describe("splitAtS") {
    describe("When n is bigger than the collection size (n=7, size=3)") {
      it("throws BadLocationException") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 7

        a[BadLocationException] must be thrownBy inputSeq.splitAtS(inputVal)
      }
    }

    describe("When n is smaller than 0 (n=-2)") {
      it("throws BadLocationException") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = -2

        a[BadLocationException] must be thrownBy inputSeq.splitAtS(inputVal)
      }
    }

    describe("When it gets en empty collection") {
      it("returns (Nil, Nil)") {
        val inputSeq = Nil
        val inputVal = 0
        val expected = (Nil, Nil)
        val actual = inputSeq.splitAtS(inputVal)

        actual mustBe expected
      }
    }
  /*
    describe("When it gets a collection with one item and (n=0)") {
      it("returns a tuple with an empty collection and a collection with the item - (Nil, (item))") {
        val inputSeq = Seq(1)
        val inputVal = 0
        val expected = (Nil, Seq(1))
        val actual = inputSeq.splitAtS(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one item and (n=1)") {
      it("returns a tuple with a collection that contains that item and an empty collection - ((item),Nil)") {
        val inputSeq = Seq(1)
        val inputVal = 1
        val expected = (Seq(1), Nil)
        val actual = inputSeq.splitAtS(inputVal)

        actual mustBe expected
      }
    }

   */

    describe("When it gets a collection of size s>0 and (n=0)") {
      it("returns a tuple with an empty collection and the input collection - (Nil, input)") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 0
        val expected = (Nil, Seq(1, 2, 3))
        val actual = inputSeq.splitAtS(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection of size s>0 and (n=s)") {
      it("returns a tuple with the input collection and an empty collection - (input,Nil)") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 3
        val expected = (Seq(1, 2, 3), Nil)
        val actual = inputSeq.splitAtS(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection and a number n smaller than the collection's size (n=3, size=7) (1, 2, 3, 4, 5, 6, 7)") {
      it("returns a tuple with the collection split at the n-th index - ((1, 2, 3),(4, 5, 6, 7))") {
        val inputSeq = Seq(1, 2, 3, 4, 5, 6, 7)
        val inputVal = 3
        val expected = (Seq(1, 2, 3), Seq(4, 5, 6, 7))
        val actual = inputSeq.splitAtS(inputVal)

        actual mustBe expected
      }
    }
  }

  describe("splitBy") {
    describe("When it gets an empty collection") {
      it("return a collection with an empty collection") {
        val inputSeq = Nil
        val inputVal = 1
        val expected = Seq(Nil)
        val actual = inputSeq.splitBy(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection with a single item, and is asked to split by that item") {
      it("returns a collection of 2 empty collections") {
        val inputSeq = Seq('1')
        val inputVal = '1'
        val expected = Seq(Nil, Nil)
        val actual = inputSeq.splitBy(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection with a repetition of a single item") {
      it("returns a collection of x empty collections (x=the size of the input collection + 1)") {
        val inputSeq = Seq('1', '1', '1')
        val inputVal = '1'
        val expected = Seq(Nil, Nil, Nil, Nil)
        val actual = inputSeq.splitBy(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection and an item that doesn't exists in the collection ('1', '1', '1'), item='2' ") {
      it("returns a collection with the input collection") {
        val inputSeq = Seq('1', '1', '1')
        val inputVal = '2'
        val expected = Seq(Seq('1', '1', '1'))
        val actual = inputSeq.splitBy(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection and an item that exists in it - ('1', '2', '1', '2', '1', '1'), item='2'") {
      it("returns a collection with the collections of the items in between the start, the instances of the item, and the end of the collection (Seq('1'), Seq('1'), Seq('1', '1')) ") {
        val inputSeq = Seq('1', '2', '1', '2', '1', '1')
        val inputVal = '2'
        val expected = Seq(Seq('1'), Seq('1'), Seq('1', '1'))
        val actual = inputSeq.splitBy(inputVal)

        actual mustBe expected
      }
    }
  }

  describe("rotateLeft") {
    describe("When it gets an empty collection") {
      it("returns an empty collection") {
        val inputSeq = Nil
        val inputVal = 4
        val expected = Nil
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection and n=0") {
      it("returns the input collection") {
        val inputSeq = Seq(1, 2, 3, 4)
        val inputVal = 0
        val expected = Seq(1, 2, 3, 4)
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection with one item") {
      it("returns the input collection") {
        val inputSeq = Seq(1)
        val inputVal = 999
        val expected = Seq(1)
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }

    describe("When n is bigger than the collection size (n=5, size=3) (1, 2, 3)") {
      it("returns a collection shifted n places to the left (3, 1, 2)") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 5
        val expected = Seq(3, 1, 2)
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection of length s and n is negative (n = -1)") {
      it("returns a collection shifted n places to the right") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = -1
        val expected = Seq(3, 1, 2)
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }

    describe("When n is negative and abs(n) is larger than the collection size (n = -4, size = 3) (1, 2, 3)") {
      it("returns a collection shifted n places to the right (3, 1, 2)") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = -4
        val expected = Seq(3, 1, 2)
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }

    describe("When it gets a collection of size s, and a number n < s (n=1, size=5) (1, 2, 3, 4, 5)") {
      it("returns a collection shifted n places to the left (2, 3, 4, 5, 1)") {
        val inputSeq = Seq(1, 2, 3, 4, 5)
        val inputVal = 1
        val expected = Seq(2, 3, 4, 5, 1)
        val actual = inputSeq.rotateLeft(inputVal)

        actual mustBe expected
      }
    }
  }
  /*
  describe("When n is bigger than the collection size (n=5, size=3) (1, 2, 3)") {
    it("returns a collection with the last item at the beginning (3, 1, 2)") {
      val inputSeq = Seq(1, 2, 3)
      val inputVal = 5
      val expected = Seq(3, 1, 2)
      val actual = inputSeq.rotateLeft(inputVal)

      actual mustBe expected
    }
  }
  describe("When n is negative (n = -1)") {
    it("returns a collection with the last item at the beginning") {
      val inputSeq = Seq(1, 2, 3)
      val inputVal = -1
      val expected = Seq(3, 1, 2)
      val actual = inputSeq.rotateLeft(inputVal)

      actual mustBe expected
    }
  }
  describe("When n is negative and abs(n) is larger than the collection size (n = -4, size = 3) (1, 2, 3)") {
    it("returns a collection with the last item at the beginning (3, 1, 2)") {
      val inputSeq = Seq(1, 2, 3)
      val inputVal = -4
      val expected = Seq(3, 1, 2)
      val actual = inputSeq.rotateLeft(inputVal)

      actual mustBe expected
    }
  }
  describe("When it gets a collection, and a number n (n=1, size=5) (1, 2, 3, 4, 5)") {
    it("returns a collection with the original's first item at the end of the collection (2, 3, 4, 5, 1)") {
      val inputSeq = Seq(1, 2, 3, 4, 5)
      val inputVal = 1
      val expected = Seq(2, 3, 4, 5, 1)
      val actual = inputSeq.rotateLeft(inputVal)

      actual mustBe expected
    }
  }
}

   */
  describe("pick") {
    describe("When it gets an empty collection to pick from") {
      it("returns an empty collection") {
        val inputSeq = Nil
        val inputVal = 2
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected

      }
    }

    describe("When it gets a negative number") {
      it("returns an empty collection") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = -2
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected
      }
    }

    describe("When it gets (n=0)") {
      it("returns an empty collection") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 0
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected
      }
    }

    describe("When it gets (n=1)") {
      it("returns a collection with 1 item") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 1
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected
      }
    }

    describe("When it gets n equal to the collection size (n=s)") {
      it("returns a shuffled version of the input collection") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 3
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected
      }
    }

    describe("When it gets a number n bigger than the collection size (n=5, size=3) (1,2,3)") {
      it("returns a shuffled version of the input collection") {
        val inputSeq = Seq(1, 2, 3)
        val inputVal = 5
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected
      }
    }

    describe("When it gets a collection of size s and a number n < s (n=3, size=5) (1,2,3,4,5)") {
      it("returns n random items from the collections") {
        val inputSeq = Seq(1, 2, 3, 4, 5)
        val inputVal = 3
        val actual = inputSeq.pick(inputVal)
        val expected = actual.intersect(inputSeq)

        actual mustBe expected
      }
    }
  }
}


