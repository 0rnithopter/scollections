package gradleTest7

import scala.util.Random

class BadLocationException extends Throwable

class EmptyCollectionException extends Throwable

class BadItemException extends Throwable

object SCollections {

  def main(args: Array[String]): Unit = print("hello maagar")


  implicit class Decode[A](val seq: Seq[(Int, A)]) {
    def decode: Seq[A] = {
      seq
        .flatMap { case (count, item) => Seq.fill(count)(item) }
    }
  }

  implicit class Functions[T](val seq: Seq[T]) {
    def isPalindrome: Boolean = {
      seq == seq.reverse
    }

    def compress: Seq[T] = {
      seq.pack
        .map(_.head)
    }

    def pack: Seq[Seq[T]] = {
      val switches = seq.indices
        .slice(1, seq.length)
        .filter(i => seq(i - 1) != seq(i))

      val starts = 0 +: switches
      val finishes = switches :+ seq.length

      starts.zip(finishes)
        .filter { case (x, y) => x != y }
        .map { case (from, until) => seq.slice(from, until) }
    }

    def encode: Seq[(Int, T)] = {
      seq.pack
        .map(x => (x.length, x.head))
    }

    def decode[A](seq: Seq[(Int, A)]): Seq[A] = {
      seq
        .flatMap { case (count, item) => Seq.fill(count)(item) }
    }

    def duplicate: Seq[T] = {
      seq
        .flatMap(x => Seq(x, x))
    }

    def dropS(n: Int): Seq[T] = {
      seq.zipWithIndex
        .filter { case (_, index) => index % n != n - 1 }
        .map { case (value, _) => value }
    }

    def splitAtS(loc: Int): (Seq[T], Seq[T]) = {
      if (loc < 0 || loc > seq.length) {
        throw new BadLocationException
      } else {
        //println(seq.length)
        (seq.slice(0, loc), seq.slice(loc, seq.length))
      }
    }

    def splitBy(item: T): Seq[Seq[T]] = {
      val indexes = seq.zipWithIndex
        .filter { case (value, _) => value == item }
        .map { case (_, index) => index }

      val start = 0 +: indexes.map(_ + 1)
      val end = indexes :+ seq.length

      val intervals = start.zip(end)
      intervals.map { case (from, until) => seq.slice(from, until) }
    }

    def rotateLeft(num: Int): Seq[T] = {
      val size = seq.size
      val newNum = size match {
        case 0 => 0
        case _ if num < 0 => num % size + size
        case _ => num % size
      }

      val (start, finish) = seq.splitAtS(newNum)
      finish ++ start
    }

    def pick(num: Int): Seq[T] = {
      Random.shuffle(seq).take(num)
    }
  }

}